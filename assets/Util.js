class Util {
    constructor() {
    }
    shuffleList() {
        let tmp, randomIndex;
        for(var index=0;index<this.list.length;index++) {
            randomIndex = Math.floor( Math.random() * this.list.length);
            //swap elements
            tmp = this.list[index];
            this.list[index] = this.list[randomIndex];
            this.list[randomIndex] = tmp;
        }
    }
    setList(list) {
        this.list = list;
    }
    getList() {
        return this.list;
    }
}

export default Util;